import requests
import re

import config
from util import myinfo, mywarn
from threading import Lock
import traceback

class State:
    pass

def init(id, proxies):
    global state
    state = State()

    state.id = id

    state.cookies = {}
    state.proxies = proxies


    state.headers = config.BROWSER_HEADERS

    state.session = requests.Session()
    state.lock = Lock()

def get(url):
    with state.lock:
        myinfo(" GET: ", url)

        try:
            r = state.session.get(url,
                     verify=False,
                     headers=state.headers,
                     proxies = state.proxies,
                     timeout = 10)
        except Exception as e:
            #myerror(traceback.format_exc())
            mywarn("GET Exception: ", e)
            r = None

    if r != None:
        myinfo(" GET: Complete")
    return r

def postMulti(url, postData, moreHeaders = {}):
    with state.lock:
        myinfo(" POST_MULTI: ", url)
        headers = {**state.headers, **moreHeaders}

        try:
            r = state.session.post(url,
                          verify=False,
                          headers = headers,
                          proxies=state.proxies,
                          files=postData,
                          timeout=10,
                          allow_redirects=True
                               )
        except Exception as e:
            # myerror(traceback.format_exc())
            mywarn("POST_MULTI Exception: ", e)
            r = None

    if r != None:
        myinfo(" POST_MULTI: Complete")
    return r



def post(url, postData, moreHeaders = {}):
    with state.lock:
        myinfo(" POST: ", url)

        headers = {**state.headers, **moreHeaders}

        try:
            r = state.session.post(url,
                          verify=False,
                          headers = headers,
                          proxies=state.proxies,
                          data=postData,
                          timeout=10
                               )
        except Exception as e:
            # myerror(traceback.format_exc())
            mywarn("POST Exception: ", e)
            r = None

    if r != None:
        myinfo(" POST: Complete")
    return r




