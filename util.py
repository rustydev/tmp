from time import localtime, strftime
import sys

from bs4 import BeautifulSoup

def myprint(*arg):
    now = strftime("%Y-%m-%d %H:%M:%S ", localtime())
    print(now, end='')


    print('', ': ', end='')
    for i in arg:
        print(i,  end='')

    print("")

    sys.stdout.flush()


def myinfo(*arg):
    myprint(*arg)

def mytrace(*arg):
    myprint(*arg)

def mywarn(*arg):
    myprint(*arg)

def myerror(*arg):
    myprint(*arg)
    myprint("Exiting")
    exit(1)

def myprintAlive():
    sys.stdout.write('.')
    sys.stdout.flush()

 # ugly but clear
def loanDiff(loan1, loan2):
    if loan1['rate'] != loan2['rate']:
        # myprint("New rate")
        return True

    if loan1['risk'] != loan2['risk']:
        # myprint("New risk")
        return True

    if loan1['term'] != loan2['term']:
        # myprint("New term")
        return True

    if loan1['funded'] != loan2['funded']:
        # myprint("New funded")
        return True

    return False





def getLoginFormIds(text):
#    mysearch = re.search('Your Available Funds &pound;(\d*,?\d+\.\d\d)', text)


    soup = BeautifulSoup(text, 'html.parser')

    #myprint(soup.prettify())

    for elem in soup.find_all('input'):
        #print(elem['name'], elem)
        if elem['name'] == '_csrf_token':
            _csrf = elem['value']
            #print("_csrf_token is ", _csrf)

    return {"_csrf_token" : _csrf}